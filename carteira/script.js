document.addEventListener("DOMContentLoaded", function () {
    const base_url = window.location.origin + "/";
    const urlParams = new URLSearchParams(window.location.search);
    const urlDigest = urlParams.get("digest");

    const nameElem = document.getElementById("name");
    const courseElem = document.getElementById("course");
    const raElem = document.getElementById("ra");
    const expireElem = document.getElementById("expire");
    const photoElem = document.getElementById("photo");
    const qrElem = document.getElementById("qr");
    const validadeElem = document.getElementById("validade");

    var qrcode = new QRious({
            element: document.getElementById("qr"),
            background: '#ffffff',
            foregroundAlpha: 1,
            level: 'H',
            size: 256,
            value: base_url + "/validar?digest="+urlDigest
    });

    const loadFromServer = async () => {
        console.log("Request: checkcert/"+urlDigest);
        const response = await fetch(base_url + "checkcert/" + urlDigest);
        const json = await response.json();
        console.log(":: Json Response ::")
        console.log("uid: " + json.uid);
        console.log("ra: " + json.ra);
        console.log("name: " + json.name);
        console.log("course: " + json.course);
        console.log("pfp: " + json.pfp);
        console.log("is_valid: " + json.is_valid);
        console.log("uts: " + json.uts);
        console.log(":: :: :: :: ::");

        var generatedDate = new Date(json.uts).toLocaleDateString("pt-BR")
        var expireDate = new Date(json.uts + 604800000).toLocaleDateString("pt-BR")

        raElem.textContent = json.ra;
        nameElem.textContent = json.name;
        courseElem.textContent = json.course;
        validadeElem.textContent = "Carteira gerada " + generatedDate + ", valida até " + expireDate;

        var qrcode = new QRious({
            element: document.getElementById("qr"),
            background: '#ffffff',
            foregroundAlpha: 1,
            level: 'H',
            size: 256,
            value: base_url + "/validar?digest="+urlDigest
        });

        photoElem.src = base_url + json.pfp
    }

    loadFromServer()

});

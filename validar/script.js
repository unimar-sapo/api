document.addEventListener("DOMContentLoaded", function () {
    const base_url = window.location.origin + "/";
    const urlParams = new URLSearchParams(window.location.search);
    const urlDigest = urlParams.get("digest");

    const loadFromServer = async () => {
        const response = await fetch(base_url + "checkcert/" + urlDigest);
        const json = await response.json();
        console.log(json.uid);
        console.log(json.ra);
        console.log(json.name);
        console.log(json.course);
        console.log(json.is_valid);

        const ra = document.getElementById("ra");
        const aluno = document.getElementById("aluno");
        const curso = document.getElementById("curso");
        const digest = document.getElementById("digest");

        const valid = document.getElementById("valid");
        const invalid = document.getElementById("invalid");

        ra.textContent = "" + json.ra;
        aluno.textContent = "" + json.name;
        curso.textContent = "" + json.course;
        digest.textContent = urlDigest;

        if(json.is_valid) {
            valid.classList.remove('invisible');
        } else {
            invalid.classList.remove('invisible');
        }
    }

    loadFromServer()

});

#[macro_use] extern crate rocket;

use api::*;
use std::io::{stdin, Read};
use std::fs::File;
use rocket::data::{Data, ToByteUnit};
use std::io;
use rocket::fs::{FileServer, relative};
use rocket::{Request, Response};
use rocket::fairing::{Fairing, Info, Kind};

use std::env;
use std::fs;

use rocket::fs::TempFile;
use uuid::Uuid;
use rocket::form::Form;
use rocket::http::Header;

use api::models::User;

use rocket::fs::NamedFile;
use std::path::PathBuf;
use std::path::Path;

#[post("/")]
fn root() -> &'static str {
    "Success!\n"
}

#[post("/newuser?<ra>&<name>&<course>&<courseYear>&<courseId>")]
fn newuser(
    ra: i32,
    name: &str,
    course: &str,
    courseYear: &str,
    courseId: i32
) -> &'static str {
    println!("ra: {}, name: {}, course: {}, courseYear: {}, courseId: {}", ra, name, course, courseYear, courseId);
    let connection = &mut establish_connection();
    create_user(connection, &ra, name, course, courseYear, &courseId);
    "Success!\n"
}

// #[get("/updatepic?<ra>&<pictureurl>")]
// fn updatepic(
//     ra: i32,
//     pictureurl: &str
// ) -> &'static str {
//     println!("ra: {}, pictureurl: {}", ra, pictureurl);
//     let connection = &mut establish_connection();
//     update_img(connection, &ra, pictureurl);
//     "Success!\n"
// }

#[derive(FromForm)]
struct Upload<'f> {
    file: TempFile<'f>
}

#[post("/upload?<ra>", format = "multipart/form-data", data = "<form>")]
async fn upload(mut form: Form<Upload<'_>>, ra: i32) -> std::io::Result<()> {

    let user_file = form.file.raw_name().unwrap().dangerous_unsafe_unsanitized_raw().as_str();
    let extension = user_file.split('.').last().unwrap();

    let new_file = format!("uploads/{}", ra.to_string() + "." + extension);

    println!("destination = {}", new_file);
    println!("length = {} bytes", form.file.len());

    // form.file.persist_to(file_name).await?;
    form.file.copy_to(new_file.clone()).await?;

    let connection = &mut establish_connection();
    update_img(connection, &ra, new_file.as_str());

    Ok(())
}

#[get("/userinfo/<ra>")]
fn userinfo(
    ra: i32
) -> String {
    let connection = &mut establish_connection();
    let user = user_details(connection, &ra);
    serde_json::to_string(&user).unwrap()
}

#[get("/uploads/<target>")]
async fn load_from_uploads(
    target: PathBuf
) -> NamedFile {
    NamedFile::open(Path::new("uploads/").join(target)).await.ok().expect("Error")
}

#[post("/certificate/<ra>")]
fn certificate(ra: i32) -> String  {
    println!("ra: {}", ra);
    let connection = &mut establish_connection();
    let uuid = create_certificate(connection, &ra);
    format!("{{ \"uuid\": \"{}\" }}", uuid)
}

#[get("/checkcert/<uuid>")]
fn checkcert(uuid: &str) -> String {
    println!("uuid: {}", uuid);
    let connection = &mut establish_connection();
    let cert = certificate_details(connection, &uuid);
    let uts = cert.uts;
    let ra = cert.ra;

    let user = user_details(connection, &ra);
    let user_name = user.name;
    let user_course = user.course;
    let pfp = user.picUrl;

    let time_threshold = 30000; // 10s

    use std::time::UNIX_EPOCH;
    use std::time::SystemTime;

    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards")
        .as_millis();

    let now_uts = since_the_epoch as i64;

    let is_valid = uts + time_threshold >= now_uts;

    format!(
"{{
    \"uuid\": \"{}\",
    \"ra\": {},
    \"name\": \"{}\",
    \"course\": \"{}\",
    \"pfp\": \"{}\",
    \"uts\": {},
    \"is_valid\": {}
}}", uuid, ra, user_name, user_course, pfp, uts ,is_valid
    )
}

/// Catches all OPTION requests in order to get the CORS related Fairing triggered.
#[options("/<_..>")]
fn all_options() {
    /* Intentionally left empty */
}

pub struct Cors;

#[rocket::async_trait]
impl Fairing for Cors {
    fn info(&self) -> Info {
        Info {
            name: "Cross-Origin-Resource-Sharing Fairing",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Methods",
            "POST, PATCH, PUT, DELETE, HEAD, OPTIONS, GET",
        ));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
    }
}
#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(Cors)
        .mount("/", routes![root])
        .mount("/", routes![newuser])
        .mount("/", routes![upload])
        .mount("/", routes![userinfo])
        .mount("/", routes![load_from_uploads])
        .mount("/", routes![certificate])
        .mount("/", routes![checkcert])
        .mount("/carteira", FileServer::from(relative!("/carteira")))
        .mount("/validar", FileServer::from(relative!("/validar")))
}

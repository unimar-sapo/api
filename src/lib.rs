pub mod models;
pub mod schema;

use diesel::sqlite::SqliteConnection;
use diesel::prelude::*;
use dotenvy::dotenv;
use std::env;

use std::time::{SystemTime, UNIX_EPOCH};
use uuid::Uuid;

// Connection
pub fn establish_connection() -> SqliteConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

// Operations
use self::models::{ NewUser, User, NewCertificate, Certificate };

pub fn create_user(
    conn: &mut SqliteConnection, 
    ra: &i32, 
    name: &str, 
    course: &str, 
    courseYear: &str, 
    courseId: &i32
) -> usize {

    use crate::schema::users;
    let new_user = NewUser { ra, name, course, courseYear, courseId };

    diesel::insert_into(users::table)
        .values(&new_user)
        .execute(conn)
        .expect("Error")
}

pub fn update_user(
    conn: &mut SqliteConnection, 
    target_ra: &i32,
    new_name: &str,
    new_course: &str,
    new_courseYear: &str,
    new_courseId: &i32
) -> usize {

    use self::schema::users::dsl::{ users };
    use crate::schema::users::*;

    diesel::update(users.find(target_ra))
        .set((name.eq(name), course.eq(new_course), courseYear.eq(new_courseYear), courseId.eq(new_courseId)))
        .execute(conn)
        .expect("Error")
}

pub fn update_img(
    conn: &mut SqliteConnection,
    target_ra: &i32,
    newPicUrl: &str
) -> usize {

    use self::schema::users::dsl::{ users };
    use crate::schema::users::*;

    diesel::update(users.find(target_ra))
        .set(picUrl.eq(newPicUrl))
        .execute(conn)
        .expect("Error")
}

pub fn user_details(
    conn: &mut SqliteConnection,
    target_ra: &i32,
) -> User {
    use self::schema::users::dsl::users;
    users
        .find(target_ra)
        .select(User::as_select())
        .first(conn)
        .expect("Error")
}

pub fn create_certificate(
    conn: &mut SqliteConnection,
    ra: &i32
) -> String {

    let uuid = format!("{}", Uuid::new_v4());

    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards")
        .as_millis();

    let uts = since_the_epoch as i64;

    use crate::schema::certificates;
    let new_certificate = NewCertificate { uuid: &uuid, uts: &uts, ra: ra };

    diesel::insert_into(certificates::table)
        .values(&new_certificate)
        .execute(conn)
        .expect("Error");

    uuid
}

pub fn certificate_details(
    conn: &mut SqliteConnection,
    uuid: &str,
) -> Certificate {
    use self::schema::certificates::dsl::certificates;
    certificates
        .find(uuid)
        .select(Certificate::as_select())
        .first(conn)
        .expect("Error")
}

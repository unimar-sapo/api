// @generated automatically by Diesel CLI.

diesel::table! {
    certificates (uuid) {
        uuid -> Text,
        uts -> BigInt,
        ra -> Integer,
    }
}

diesel::table! {
    users (ra) {
        ra -> Integer,
        name -> Text,
        course -> Text,
        courseYear -> Text,
        courseId -> Integer,
        picUrl -> Text,
    }
}

diesel::allow_tables_to_appear_in_same_query!(
    certificates,
    users,
);

use self::models::*;
use diesel::prelude::*;
use diesel_demo::*;

fn main() {
    use self::schema::users::dsl::*;

    let connection = &mut establish_connection();
    let results = users
        .filter(published.eq(true))
        .limit(5)
        .select(User::as_select())
        .load(connection)
        .expect("Error loading users");

    println!("Displaying {} posts", results.len());
    for post in results {
        println!("{}", user.ra);
        println!("-----------\n");
        println!("{}", user.name);
    }
}

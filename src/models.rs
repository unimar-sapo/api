use diesel::prelude::*;
use serde::Deserialize;
use serde::Serialize;

#[derive(Queryable, Selectable, Serialize, Deserialize)]
#[diesel(table_name = crate::schema::users)]
#[diesel(check_for_backend(diesel::sqlite::Sqlite))]
pub struct User {
    pub ra: i32,
    pub name: String,
    pub course: String,
    pub courseYear: String,
    pub courseId: i32,
    pub picUrl: String
}

use crate::schema::users;
#[derive(Insertable)]
#[diesel(table_name = users)]
pub struct NewUser<'a> {
    pub ra: &'a i32,
    pub name: &'a str,
    pub course: &'a str,
    pub courseYear: &'a str,
    pub courseId: &'a i32,
}

#[derive(Queryable, Selectable, Serialize, Deserialize)]
#[diesel(table_name = crate::schema::certificates)]
#[diesel(check_for_backend(diesel::sqlite::Sqlite))]
pub struct Certificate {
    pub uuid: String,
    pub uts: i64,
    pub ra: i32,
}

use crate::schema::certificates;
#[derive(Insertable)]
#[diesel(table_name = certificates)]
pub struct NewCertificate<'a> {
    pub uuid: &'a str,
    pub uts: &'a i64,
    pub ra: &'a i32,
}

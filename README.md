### Como subir a API (local, desenvolvimento):
1. Instalar rustup
2. Clonar repo: `git clone https://gitlab.com/unimar-sapo/api`
3. Entrar na pasta do repo: `cd api`
4. Mover para o rust nightly e atualizar: `rustup default nightly && rustup update && cargo update`
5. Iniciar API: `cargo run`

Execuções subsequentes, apenas `cargo run` dentro da pasta `api` é o suficiente para iniciar a API.

---

### Como subir a API (docker, produção):
1. Verificar Dockerfile de acordo com maquina (produção no repositorio está gerando binários para linux/arm64, mais infos de targets em `https://rust-lang.github.io/rustup-components-history/`)
2. Adaptar `private-docker-compose.yml` de acordo com as configurações da maquina de produção, reverse proxy (caso existente) e derivados..
3. Renomear `private-docker-compose.yml` para `docker-compose.yml`
4. Rodar `./build-image.sh`

** build-image.sh deleta o banco de dados existente, cria um novo, constrói imagem do docker, e sobe container com docker compose.

Execuções subsequentes, apenas `./build-image.sh` é o suficiente para construir uma nova imagem com as novas alterações e subir a API.

---

### Endpoints:

   - (root) POST /
   
   - (upload) POST /upload?<ra> multipart/form-data
   
   - (newuser) POST /newuser?<ra>&<name>&<course>&<courseYear>&<courseId>
   
   - (userinfo) GET /userinfo/<ra>
   
   - (load_from_uploads) GET /uploads/<target>
   
   - (certificate) POST /certificate/<ra>
   
   - (checkcert) GET /checkcert/<uuid>
   
   - (FileServer: validar) GET /validar/<path..> [10]
   
   - (FileServer: carteira) GET /carteira/<path..> [10]
   

** Esquema do banco de dados está disponível em `migrations/data_users/up.sql`

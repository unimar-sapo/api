FROM rustlang/rust:nightly as builder

RUN apt-get update
RUN apt-get -y install upx musl-tools
RUN rustup target add aarch64-unknown-linux-gnu
WORKDIR /api
COPY . .
RUN cargo build --release --target aarch64-unknown-linux-gnu

RUN ls -lah target/aarch64-unknown-linux-gnu/release/api
RUN strip target/aarch64-unknown-linux-gnu/release/api
# RUN upx --ultra-brute target/aarch64-unknown-linux-gnu/release/api
RUN ls -lah target/aarch64-unknown-linux-gnu/release/api

FROM ubuntu:focal

COPY --from=builder /api/target/aarch64-unknown-linux-gnu/release/api /api/api
CMD ["chmod +x /api/api"]
RUN apt-get update
RUN apt-get -y install sqlite3
COPY . /api
WORKDIR /api
EXPOSE 8000
CMD ["./api"]

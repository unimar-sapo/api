Restart db:
diesel migration run && diesel migration redo

POST / - root endpoint
curl -X POST localhost:9090

POST /newuser?<user> - Creates new user into database
curl -X POST 'localhost:9090/newuser?ra=1111&name=John%20doe&course=ADS&courseYear=2023&courseId=2222'

POST /upload?<ra>&<file> - Upload profile picture and write to database
curl -F "file=@/home/vega/2023-08-29_18:37:45_screen.png" 127.0.0.1:9090/upload?ra=1111

GET /userinfo/<ra> - Retrieves user info
curl "localhost:9090/userinfo/1111"

GET /uploads/<target> - Retrieve file from uploads
curl "localhost:9090/uploads/1111.png"

GET /carteira - Generates card by given params
http://localhost:9090/carteira?name=John%20Doe&course=ADS&ra=11111&expire=11/11&photo=carteira/placeholder.png&qr=carteira/placeholder.png

GET /carteira - Generates card by given params
http://localhost:9090/carteira?name=John%20Doe&course=ADS&ra=11111&expire=11/11&photo=carteira/placeholder.png&qr=carteira/placeholder.png

POST /certificate/<ra> - Generetes a cert for a card
curl -X POST "localhost:9090/certificate/1111"

GET /checkcert/<cert_uuid> - Check if cert is valid
curl "localhost:9090/checkcert/38db293d-09b2-40f4-8f91-2c8b6dc9cefe"

GET /validar?digest=<cert_uuid> - Frontend for checking if cert is valid
curl "localhost:9090/validar?digest=38db293d-09b2-40f4-8f91-2c8b6dc9cefe"
